1997
� ������������ ������ ������ 4 �������� ������������ ������� � ���������� �����-
����� �������� �� ����� ������������� ������������� � ���������������� ����������
������������ � �������� �� ������ ��������, �������������� � ���������� �� ����������
��������� ���������� � ��������� ������������ ������ � ������ ������������ ��������.
� ��������� �������� ����������:
- ����������� �� ��������� �������� �������� ������������������� �������������-
�� ����� ������������ � ��û �.�. ������� (12.08.1997 � 22.12.2001);
- ������ ����������� ����� �������� �. ���. �., ��������� �.�. �������� (04.08.1997 �
31.05.1999).
� �������� ��� ��������, ������������ ��� ��������, �.�.�., ��������� �.�. �������� ���-
��� ���������� ���������� �������� ������ � ������ ������ ��������� ���������� ��������.
� � ����� ������ ������������� ������� �� ��������� ����������� �������� ���-
����� � ������ ����� � ����������� ��������.
� ������� 1-� ������������� �����-������� �� �������������� ���������� ��� ���-
������ � ���������� � ����������� ��������������� ��������� ����� (������������ � ��-
�������� �������� ���������������� �������������� ����������, �.�.�., ��������� �.�.
��������).
� ��������� ������ ��������� � ����������������� ������� ������ (�����) �� ���-
������� ������������������ ��������.