# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import TemplateView


class IndexView(TemplateView):
    template_name = 'index.html'


class TimeLineView(TemplateView):
    template_name = 'timeline.html'

    def get_context_data(self, **kwargs):
        context = super(TimeLineView, self).get_context_data(**kwargs)
        context['active_nav_button'] = 'timeline'
        return context


class RectorsView(TemplateView):
    template_name = 'rectors.html'

    def get_context_data(self, **kwargs):
        context = super(RectorsView, self).get_context_data(**kwargs)
        context['active_nav_button'] = 'rectors'
        return context


class SymbolicsView(TemplateView):
    template_name = 'symbolics.html'

    def get_context_data(self, **kwargs):
        context = super(SymbolicsView, self).get_context_data(**kwargs)
        context['active_nav_button'] = 'symbolics'
        return context